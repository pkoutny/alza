import {
  CssBaseline,
  ThemeProvider,
  unstable_createMuiStrictModeTheme as createTheme,
} from '@material-ui/core'

const theme = createTheme({
  palette: {
    primary: {
      main: '#3d8af7',
    },
    background: { default: '#eee' },
    divider: '#000',
  },
  typography: {
    h1: {
      fontSize: '2rem',
    },
  },
})

const Theme: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    {children}
  </ThemeProvider>
)

export default Theme
