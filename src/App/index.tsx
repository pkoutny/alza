import React from 'react'
import Category from './Category'

const App: React.FC = () => (
  <>
    <Category />
  </>
)

export default App
