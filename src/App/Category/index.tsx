import React from 'react'
import Carousel from '../../components/Carousel'
import { TTabCallback } from '../../components/Tabs'
import { ISorting, useProducts } from './../../hooks/useProducts'
import Items from './Items'
import Layout, { ILayoutProps } from './Layout'
import Subcategories, { ISubcategoriesProps } from './Subcategories'

const category: {
  subcategories: ISubcategoriesProps['subcatgories']
  title: ILayoutProps['title']
} = {
  title: 'Notebooky',
  subcategories: [
    { label: 'Macbook' },
    { label: 'Herní' },
    { label: 'Kancelářské' },
    { label: 'Profesionální' },
    { label: 'Stylové' },
    { label: 'Základní' },
    { label: 'Dotykové' },
    { label: 'Na splátky' },
    { label: 'VR Ready' },
    { label: 'IRIS Graphics' },
    { label: 'Brašny, batohy' },
    { label: 'Příslušenství' },
  ],
}

const Category: React.FC = () => {
  const [sorting, setSorting] = React.useState<ISorting>({})
  const [activeTab, setActiveTab] = React.useState<string | undefined>()

  const data = useProducts(sorting)

  const sortHandler = React.useCallback(
    (options?: ISorting) =>
      ({ label }: TTabCallback) => {
        setSorting(options || {})
        setActiveTab(label)
      },
    [],
  )

  return (
    <Layout title={category.title}>
      <Subcategories subcatgories={category.subcategories} />
      <Carousel title="Nejprodávanější" items={data.slice(0, 10)} />
      <Items
        items={data}
        activeTab={activeTab}
        tabs={[
          {
            label: 'TOP',
            onClick: sortHandler(),
          },
          {
            label: 'Nejprodávanější',
            onClick: sortHandler(),
          },
          {
            label: 'Od nejlevnějšího',
            onClick: sortHandler({ sortBy: 'PRICE', direction: 'ASC' }),
          },
          {
            label: 'Od nejdražšího',
            onClick: sortHandler({ sortBy: 'PRICE', direction: 'DESC' }),
          },
        ]}
      />
    </Layout>
  )
}

export default Category
