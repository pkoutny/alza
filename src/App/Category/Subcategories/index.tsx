import Grid from '@material-ui/core/Grid'
import React from 'react'
import { SubcategoryButton, useStyles } from './styles'
import { ISubcategoriesProps, TSubcategory } from './types'

export * from './types'

const Subcategory: React.FC<TSubcategory> = ({ label }) => (
  <SubcategoryButton
    variant="contained"
    fullWidth
    size="small"
    disableElevation
    color="primary"
  >
    {label}
  </SubcategoryButton>
)

const Subcategories: React.FC<ISubcategoriesProps> = ({ subcatgories }) => {
  const classes = useStyles()

  return (
    <Grid container spacing={1} className={classes.container}>
      {subcatgories.map((subcategory) => (
        <Grid item key={subcategory.label} xs={6} sm={2}>
          <Subcategory {...subcategory} />
        </Grid>
      ))}
    </Grid>
  )
}

export default Subcategories
