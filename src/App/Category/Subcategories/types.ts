export type TSubcategory = {
  label: string
}

export interface ISubcategoriesProps {
  subcatgories: TSubcategory[]
}
