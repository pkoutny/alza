import Button from '@material-ui/core/Button'
import { makeStyles, styled } from '@material-ui/core'

export const SubcategoryButton = styled(Button)(({ theme }) => ({
  margin: theme.spacing(0.5),
  display: 'inline-flex',
  flex: '1',
  whiteSpace: 'nowrap',
}))

export const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2),
  },
}))
