import React from 'react'

export interface IImageProps {
  src: string | undefined
  alt: string | undefined
  className?: string
}

const Image: React.FC<IImageProps> = ({ src, alt, children, className }) => (
  <div className={className} style={{ position: 'relative' }}>
    {src ? (
      <img src={src} alt={alt!} />
    ) : (
      <div
        style={{ width: '100%', minHeight: '10rem', backgroundColor: '#ccc' }}
      />
    )}
    {children ? (
      <div style={{ position: 'absolute', bottom: 0, left: 0 }}>{children}</div>
    ) : null}
  </div>
)

export default Image
