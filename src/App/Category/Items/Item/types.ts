import { TProduct } from '../../../../hooks/useProducts/types'

export type TItem = Pick<
  TProduct,
  | 'avail'
  | 'id'
  | 'img'
  | 'name'
  | 'price'
  | 'priceWithoutVat'
  | 'rating'
  | 'spec'
  | 'url'
>
