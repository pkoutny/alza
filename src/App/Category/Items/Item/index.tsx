import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import React from 'react'
import Rating from '../../../../components/Rating'
import SplitButton from '../../../../components/SplitButton'
import Image from './Image'
import Price from './Price'
import { StyledItem, useStyles } from './styles'
import { TItem } from './types'

export * from './types'

const Item: React.FC<TItem> = ({
  avail,
  id,
  img,
  name,
  price,
  spec,
  priceWithoutVat,
  rating,
  url,
}) => {
  const classes = useStyles()

  return (
    <StyledItem>
      <div>
        <Typography
          variant="subtitle1"
          component={Link}
          className={classes.name}
          href={url}
        >
          {name}
        </Typography>
        <Typography variant="caption" className={classes.spec}>
          {spec}
        </Typography>
      </div>
      <div className={classes.bottom}>
        <Image src={img} alt={name}>
          <Rating rating={rating} />
        </Image>
        <div className={classes.priceButton}>
          <Price price={price} priceWithoutVat={priceWithoutVat} />
          <div className={classes.buttonContainer}>
            <SplitButton
              label="Koupit"
              options={[
                { label: 'Koupit zrychleně' },
                { label: 'Porovnat' },
                { label: 'Hlídat' },
                { label: 'Přidat do seznamu' },
              ]}
            />
          </div>
        </div>
        <div className={classes.availability}>{avail}</div>
      </div>
    </StyledItem>
  )
}

export default Item
