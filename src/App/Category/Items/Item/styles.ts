import { makeStyles, styled } from '@material-ui/core'

export const useStyles = makeStyles((theme) => ({
  name: { display: 'block' },
  spec: { display: 'block' },
  priceButton: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  buttonContainer: {
    alignSelf: 'center',
  },
  price: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(0.5),
    '& > span:first-child': {
      color: 'green',
    },
    '& > span:not(:first-child)': {
      fontSize: '0.8rem',
    },
  },
  availability: {
    display: 'flex',
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  bottom: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
}))

export const StyledItem = styled('div')(({ theme }) => ({
  margin: theme.spacing(0.5),
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  justifyContent: 'space-between',
}))
