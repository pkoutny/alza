import React from 'react'
import { useStyles } from './styles'

const Price: React.FC<{ priceWithoutVat: string; price: string }> = ({
  price,
  priceWithoutVat,
}) => {
  const classes = useStyles()

  return (
    <div className={classes.price}>
      <span>{priceWithoutVat}</span>
      <span>{price}</span>
    </div>
  )
}

export default Price
