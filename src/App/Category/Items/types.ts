import { TItem } from './Item'
import { ITab } from '../../../components/Tabs'

export interface IItemsProps {
  items: TItem[]
  tabs: ITab[]
  activeTab?: string
}
