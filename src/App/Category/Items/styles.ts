import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  itemsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingLeft: theme.spacing(0.5),
    paddingRight: theme.spacing(0.5),
  },
}))
