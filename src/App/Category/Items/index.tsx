import React from 'react'
import Tabs from '../../../components/Tabs'
import Item from './Item'
import { useStyles } from './styles'
import { IItemsProps } from './types'

export * from './types'

const Items: React.FC<IItemsProps> = ({ items, tabs, activeTab }) => {
  const classes = useStyles()

  return (
    <div>
      <Tabs active={activeTab || tabs[0].label} tabs={tabs} />
      <div className={classes.itemsContainer}>
        {items.map((item) => (
          <Item key={item.id} {...item} />
        ))}
      </div>
    </div>
  )
}

export default Items
