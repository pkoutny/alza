import React from 'react'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import Container from '@material-ui/core/Container'
import { ILayoutProps } from './types'
import { useStyles } from './styles'

export * from './types'

const Layout: React.FC<ILayoutProps> = ({ children, title }) => {
  const classes = useStyles()

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Paper square variant="outlined" className={classes.paper}>
        <Typography variant="h1" className={classes.title}>
          {title}
        </Typography>
        {children}
      </Paper>
    </Container>
  )
}

export default Layout
