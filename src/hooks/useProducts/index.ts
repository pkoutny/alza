import alzaResponse from './alza_response.json'
import { IUseProducts } from './types'
import React from 'react'

export * from './types'

export const useProducts = (options?: IUseProducts) => {
  // const postUrl = 'https://www.alza.cz/Services/RestService.svc/v2/products'
  // const postData = {
  //   filterParameters: {
  //     id: 18855843,
  //     isInStockOnly: false,
  //     newsOnly: false,
  //     wearType: 0,
  //     orderBy: 0,
  //     page: 1,
  //     params: {
  //       tId: 0,
  //       v: [],
  //     },
  //     producers: [],
  //     sendPrices: true,
  //     type: 'action',
  //     typeId: '',
  //     branchId: '',
  //   },
  // }

  const { direction, sortBy } = options || {}

  const data = React.useMemo(() => {
    if (!!sortBy)
      return alzaResponse.data.sort((a, b) =>
        direction === 'ASC'
          ? a.priceNoCurrency - b.priceNoCurrency
          : b.priceNoCurrency - a.priceNoCurrency,
      )
    return alzaResponse.data
  }, [direction, sortBy])

  return data
}
