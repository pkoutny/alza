import alzaResponse from './alza_response.json'

export type TCategoryResponse = typeof alzaResponse

export type TProduct = TCategoryResponse['data'][0]

export interface ISorting {
  sortBy?: 'PRICE'
  direction?: 'ASC' | 'DESC'
}

export interface IUseProducts extends Partial<ISorting> {}
