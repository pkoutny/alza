import { TProduct } from '../../hooks/useProducts'

export type TCarouselItem = Pick<
  TProduct,
  'name' | 'img' | 'price' | 'spec' | 'rating'
> &
  Partial<TProduct>

export interface ICarouselProps {
  items: TCarouselItem[]
  title?: string
  classes?: { title?: string }
}
