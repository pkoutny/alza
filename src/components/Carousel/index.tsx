import Link from '@material-ui/core/Link'
import { useTheme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import classnames from 'classnames'
import React from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick-theme.css'
import 'slick-carousel/slick/slick.css'
import Image from '../../App/Category/Items/Item/Image'
import Rating from '../Rating'
import { CarouselContainer, useStyles } from './styles'
import { ICarouselProps } from './types'

export * from './types'

const Carousel: React.FC<ICarouselProps> = ({
  title,
  items,
  classes: propsClasses,
}) => {
  const classes = useStyles()
  const theme = useTheme()

  return (
    <CarouselContainer>
      {title ? (
        <Typography
          variant="h6"
          className={classnames(classes.title, propsClasses?.title)}
        >
          {title}
        </Typography>
      ) : null}
      <Slider
        slidesToShow={4}
        infinite
        arrows
        responsive={[
          {
            breakpoint: theme.breakpoints.values.sm,
            settings: { slidesToShow: 1 },
          },
          {
            breakpoint: theme.breakpoints.values.md,
            settings: { slidesToShow: 2 },
          },
        ]}
      >
        {items.map((item, index) => ( // TODO Toto jsem chtěl ještě refaktorovat a vkládat přes props ~ rozbíjí se <Slider />
          <div key={item.name + index} className={classes.item}>
            <Image src={item.img} alt={item.name} className={classes.image}>
              <Rating rating={item.rating} />
            </Image>
            <Typography variant="subtitle1" component={Link} href={item.url}>
              {item.name}
            </Typography>
            <Typography variant="caption">{item.spec}</Typography>
            {item.price}
          </div>
        ))}
      </Slider>
    </CarouselContainer>
  )
}

export default Carousel
