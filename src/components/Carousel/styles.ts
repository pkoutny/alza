import { makeStyles, styled } from '@material-ui/core/styles'

export const CarouselContainer = styled('div')(({ theme }) => ({
  paddingLeft: theme.spacing(5),
  paddingRight: theme.spacing(5),
  marginTop: theme.spacing(1),
  marginBottom: theme.spacing(1),
  '& * .slick-prev:before, .slick-next:before': {
    color: theme.palette.primary.main,
  },
}))

export const useStyles = makeStyles((theme) => ({
  item: {
    padding: theme.spacing(1),
    '& > a': {
      display: 'block',
    },
  },
  image: {
    '& > img': {
      margin: 'auto',
    },
  },
  title: {
    color: theme.palette.primary.main,
  },
}))
