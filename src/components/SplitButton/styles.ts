import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  popper: {
    zIndex: theme.zIndex.tooltip,
  },
  grow: {
    transformOrigin: 'center bottom',
    '&[data-placement=bottom]': {
      transformOrigin: 'center top',
    },
  },
}))
