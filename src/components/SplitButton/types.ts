export interface ISplitButtonProps {
  label: string
  options: { label: string }[]
}
