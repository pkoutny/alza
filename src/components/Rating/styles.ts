import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles((theme) => ({
  star: {
    color: theme.palette.grey[700],
    '&[data-active=true]': {
      color: '#ffb000',
    },
  },
}))
