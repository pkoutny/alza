import StarIcon from '@material-ui/icons/Star'
import React from 'react'
import { useStyles } from './styles'
import { IRatingProps } from './types'

export * from './types'

const ratings = [1, 2, 3, 4, 5]

const Rating: React.FC<IRatingProps> = ({ rating, className }) => {
  const classes = useStyles()

  return (
    <span className={className} title={rating.toString()}>
      {ratings.map((rate) => (
        <StarIcon
          key={rate}
          className={classes.star}
          data-active={rate <= rating}
        />
      ))}
    </span>
  )
}

export default Rating
