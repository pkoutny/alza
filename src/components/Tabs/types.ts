export type TTabCallback = {
  label: string
}

export interface ITab extends TTabCallback {
  onClick?: (tab: Omit<ITab, 'onClick'>) => void | Promise<void>
}

export interface ITabsProps {
  tabs: ITab[]
  active?: string
}
