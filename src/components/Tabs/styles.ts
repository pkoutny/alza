import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  tab: {
    borderColor: theme.palette.divider,
    color: theme.palette.text.primary,
    borderStyle: 'solid',
    borderBottom: 0,
    borderLeft: '1px',
    borderRight: '1px',
    borderTop: '1px',
    borderStartStartRadius: '10px',
    borderStartEndRadius: '10px',
    display: 'inline-flex',
    padding: '0.2rem 1rem 0.2rem 1rem',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      textDecoration: 'none',
    },
  },
  tabActive: {
    backgroundColor: theme.palette.primary.light,
  },
  tabs: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
}))
