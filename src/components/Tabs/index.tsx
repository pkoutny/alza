import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import classnames from 'classnames'
import React from 'react'
import { useStyles } from './styles'
import { ITabsProps } from './types'

export * from './types'

const Tabs: React.FC<ITabsProps> = ({ tabs, active }) => {
  const classes = useStyles()

  return (
    <div className={classes.tabs}>
      {tabs.map((tab) => (
        <Typography
          key={tab.label}
          className={classnames(classes.tab, {
            [classes.tabActive]: active === tab.label,
          })}
          component={Link}
          href={`#${tab.label}`}
          onClick={() => tab.onClick && tab.onClick(tab)}
        >
          {tab.label}
        </Typography>
      ))}
    </div>
  )
}

export default Tabs
